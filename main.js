
    const containerField = document.querySelector('.wrapper');  
    const persons = document.querySelector('.containerData');  
    
    const renderNewArray = (person) => {
    const newItem = document.createElement('div');
    newItem.classList.add('container-text');
    newItem.innerHTML =`<p><h2 class="name name1"> ${person.name}</h2></p>
        <p class="dataDescription">Nickname: <br><h3 class="username">${person.username}</h3></p>
        <p class="dataDescription">Address: ${person.street}</p>
        <p class="dataDescription">Phone: ${person.phone}</p>
        <p class="dataDescription">Company name: ${person.companyName}</p>
        <img class="line-divider" src="line-divider-png-grey2665x204.png">`;
        persons.appendChild(newItem);
    }
    console.log('div');
    
    const main = (data) => {
        const newArray  = data.map(person => ({      
            id: person.id,
            name: person.name,
            username: person.username,
            street : `${person.address.street}, ${person.address.zipcode} ${person.address.city}`,
            phone: person.phone,
            companyName: person.company.name,
            }));
            newArray.map(persons => renderNewArray (persons));

        console.log(newArray)
    };  

    fetch('https://jsonplaceholder.typicode.com/users')
            .then(response => response.json())
            .then(data => {
                console.log(data)
                main(data);
    });

    